var arrayNum = [];
document.querySelector(".doiViTri").style.display = "none";

function themSo() {
  var numberEl = document.querySelector("#number");
  var number = numberEl.value * 1;

  arrayNum.push(number);
  numberEl.value = "";
  var sumOfPositiveNumber = 0;
  var countPositiveNumber = 0;
  var min = arrayNum[0];
  var content = "";
  for (var i = 0; i < arrayNum.length; i++) {
    if (arrayNum[i] > 0) {
      sumOfPositiveNumber += arrayNum[i];
      countPositiveNumber++;
    }
    if (min > arrayNum[i]) {
      min = arrayNum[i];
    }
  }
  // 1,2,3 
  content += `Mảng: ${arrayNum}
  `;
  content += `1. Tông các số dương: ${sumOfPositiveNumber}
`;
  content += `2. Số lượng số dương trong mảng: ${countPositiveNumber}
`;
  content += `3. Số Bé nhất trong mảng ${min}
  `;
  // 4 sso dương nhỏ nhất trong mảng
  if (countPositiveNumber > 0) {
    var minPositive = null;
    for (var i = 0; i < arrayNum.length; i++) {
      if (
        (arrayNum[i] > 0 && arrayNum[i] < minPositive) ||
        (minPositive == null && arrayNum[i] > 0)
      ) {
        minPositive = arrayNum[i];
      }
    }
    content += `4. Số Dương nhỏ nhất trong mảng ${minPositive}
    `;
  } else {
    content += `4. Mảng không có số dương
    `;
  }

  var indexEven = -1;
  for (var i = arrayNum.length - 1; i >= 0; i--) {
    if (arrayNum[i] % 2 == 0) {
      indexEven = i;
      break;
    }
  }
  if (indexEven != -1) {
    content += `5. Số chẵn cuối cùng là ${arrayNum[indexEven]} tại vị trí ${indexEven}
    `;
  } else {
    content += `5. Không có giá trị chẵn, vị trí: ${indexEven}
    `;
  }
  //   6/Đổi vị trí
  if (arrayNum.length >= 2) {
    document.querySelector(".doiViTri").style.display = "block";
  }
  document.querySelector("#resuft").innerText = content;
}

function doiViTriHaiSo() {
  var content1 = `6. `;
  var index1 = document.querySelector("#index1").value * 1;
  var index2 = document.querySelector("#index2").value * 1;
  if (
    index1 < arrayNum.length &&
    index2 < arrayNum.length &&
    index1 >= 0 &&
    index2 >= 0
  ) {
    var arrayTem = [];
    arrayTem = arrayNum.slice();
    var numTem = arrayTem[index1];
    arrayTem[index1] = arrayTem[index2];
    arrayTem[index2] = numTem;
    ketQuaHoanDoi = true;
    document.querySelector(
      "#resuft2"
    ).innerText = `Mảng được sắp xếp lại: ${arrayTem}`;
    content1 = `6. Mảng sau khi đổi vị trí 2 số: ${arrayTem} `;
    document.querySelector("#resuft3").innerText = content1;
    console.log("arrayNum: ", arrayNum);

    return arrayTem;
  }
}
// 7.  sắp xếp mảng tăng dần
function sapXepMangTang() {
  var content1 = `7. `;
  var arrayTem = [];
  arrayTem = arrayNum.slice();
  for (var i = 0; i < arrayTem.length; i++) {
    var min = arrayTem[i];
    for (var j = i; j < arrayTem.length; j++) {
      if (min > arrayTem[j]) {
        var tem = min;
        min = arrayTem[j];
        arrayTem[j] = tem;
      }
    }
    arrayTem[i] = min;
  }
  console.log("arrayTem: ", arrayTem);
  content1 = `7. Mảng được sắp xấp tăng dần: ${arrayTem}`;
  document.querySelector("#resuft4").innerText = content1;
  console.log("arrayNum: ", arrayNum);
}
// 8. Số nguyên tố đầu tiên trên mảng
function timSoNguyenToDauTien() {
  var contain1 = `8.`;
  var arrayTem1 = arrayNum.slice();
  var countI = 0;
  for (var i = 0; i < arrayTem1.length; i++) {
    var countNT = 0;
    for (var j = 1; j <= arrayTem1[i]; j++) {
      if (arrayTem1[i] % j == 0) {
        countNT++;
      }
    }
    if (countNT == 2) {
      contain1 = `Số nguyên tố đầu tiên của mảng là ${arrayTem1[i]} tại vị trí ${i}`;
      document.querySelector("#resuft6").innerText = contain1;
      return;
    }
    countI++;
  }
  if (countI == arrayTem1.length) {
    contain1 = `8. Mảng không có số nguyên tố, vị trí trả về -1`;
    document.querySelector("#resuft6").innerText = contain1;
    return;
  }
}

// 9> tìm số nguyên trong mảng
var arrayThuc = [];
function themSoThuc() {
  var numberEl = document.querySelector("#number-ex9");
  var number = numberEl.value * 1;

  arrayThuc.push(number);
  numberEl.value = "";
  var sum = 0;
  for (var i = 0; i < arrayThuc.length; i++) {
    if (Math.floor(arrayThuc[i]) == Math.ceil(arrayThuc[i])) {
      sum++;
    }
  }
  document.getElementById("resuft9").innerText = `Mảng: ${arrayThuc}
   9. Số lượng số nguyên: ${sum}
   `;
}

//10
function soSanhSoAmDuong() {
  var arrayTem = [];
  var countAm = 0;
  var countDuong = 0;
  arrayTem = arrayNum.slice();
  var content10 = `10.`;
  for (var i = 0; i < arrayTem.length; i++) {
    if (arrayTem[i] > 0) {
      countDuong++;
    } else if (arrayTem[i] < 0) {
      countAm++;
    }
  }
  if (countAm == countDuong) {
    content10 = `Số lượng số âm ${countAm}, số lượng số dương ${countDuong}
    Số lượng  Số âm và Số dương bằng nhau`;
  } else if (countAm < countDuong) {
    content10 = `Số lượng số âm ${countAm}, số lượng số dương ${countDuong}
    Số lượng  Số âm bé hơn Số dương `;
  } else {
    content10 = `Số lượng số âm ${countAm}, số lượng số dương ${countDuong}
    Số lượng  Số âm lơn hơn Số dương `;
  }
  document.getElementById("resuft10").innerText = content10;
}
